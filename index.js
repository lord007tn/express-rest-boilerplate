const express = require( "express" );
const app = express();
const bodyParser = require( "body-parser" );
const mongoose = require( "mongoose" );
const dotenv = require( "dotenv" );
const morgan = require( "morgan" );
const helmet = require( "helmet" );
const cors = require( "cors" );
const compression = require( "compression" );
const errorhandler = require( "errorhandler" );
//Dotenv config
dotenv.config();
//Connect to database
mongoose.set( "useNewUrlParser", true );
mongoose.set( "useFindAndModify", false );
mongoose.set( "useCreateIndex", true );
mongoose.set( "useUnifiedTopology", true );
mongoose.connect( process.env.MONGO_DB );
mongoose.connection.on( "connected", () => {
	console.log( "DB Connected" );
} );
mongoose.connection.on( "error", ( err ) => {
	console.log( "DB Connection failed with - ", err );
} );
//Import routes
const authRoutes = require( "./routes/auth.routes" );
//Middleware
app.use( helmet() );
app.use( cors() );
app.use( bodyParser.json() );
app.use( bodyParser.urlencoded( { extended : false } ) );
app.use( morgan( "dev" ) );
app.use( compression() );
if ( process.env.NODE_ENV === "development" ) {
	// only use in development
	app.use( errorhandler() );
}
//Routes middleware
app.use( "/api", authRoutes );
//Server run
const host = "0.0.0.0";
const port = 8000 || process.env.PORT;
app.listen( port, host, ()=>{
	console.log( "Server is up and running on port number " + port );
} );
